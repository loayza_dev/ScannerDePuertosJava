# Scanner de Puertos escrito en JAVA
Programa que usa sockets TCP para listar los puertos en uso por un servidor que usa TCP del ordenador local.  
Par poder comprobar el resultado se puede usar el comando "netstat", que muestra más información,  
de la siguiente forma:  

    netstat -anpt  

* -a: muestra el PID y nombre del programa al que pertenece cada socket.
* -n: Muestra direcciones numéricas en lugar de nombres de host.
* -p: muestra el PID y nombre del programa al que pertenece cada socket.
* -t: Lista todos los puertos TCP.  
  

O  

    telnet localhost {PUERTO}  






